<?php

	include './././scripts/autoresize.php';

	use yii\widgets\ActiveForm;
	use yii\helpers\Html;
	use kartik\dropdown\DropdownX;

	$request = 'admin/customers/customer/'.$id.'/'.$service.'-services';

	if($_POST){

			if($model->load(Yii::$app->request->post())){		
				
				$array = array(
				'customer_id' => $id,
				'tariff_id' => $model->tariff_id,
				'status' => $model->status,
				'description' => $model->description,
				'unit_price' => $model->price,
				'quantity' => 1,
				'login' => $model->tariff_login,
				'phone' => $model->phone,


				);


				$result =  $api->api_call_post($request, $array);

				if($result == 1){
					echo '<div class="alert alert-success">';
		      		echo '<p>Service added!</p>';
					echo '</div>';
				}
				else {
					#echo $model->status;
					echo '<div class="alert alert-error">';
		      		echo '<p>Error! Massege: '. $api->response[0]['message'].'</p>';
					echo '</div>';
				}
		}
	}
?>

<?php echo Html::beginTag('div', ['class'=>'span3']); ?> 
	<?php echo "<h4> Add ".$service." service for ID ".$id."</h4>"; ?>
	<?php $form = ActiveForm::begin()?>
	<?php echo $form->field($model, 'tariff_id')->input('text', ['value' => 1]); ?>
	<?php 
		echo $form->field($model, 'status')->dropDownList([
			'active' => 'active',
			'disabled' => 'disabled',
			'hidden' =>'hidden',
			'pending' => 'pending'

			]); 
	?>
	<?php echo $form->field($model, 'description')->input('text', ['value' => 'Some description']); ?>
	<?php echo $form->field($model, 'price')->input('text', ['value' => 100]); ?>
	<?php echo $form->field($model, 'tariff_login')->input('text', ['value' => 11111]); ?>
	<?php echo $form->field($model, 'phone')->input('text', ['value' => '+3800000']); ?>
	<?php echo Html::submitButton('Add', ['class' => 'btn btn-success']);?>
	<?php $form = ActiveForm::end()?>
<?php echo Html::endTag('div');?>