<?php
 include './././scripts/autoresize.php';

	use yii\widgets\ActiveForm;
	use yii\helpers\Html;

	$request = 'admin/customers/customer/'.$id.'/'.$service.'-services--'.$service_id;

	if($_POST){

			if($model->load(Yii::$app->request->post())){		
				
				$array = array(

				'status' => $model->status,
				'description' => $model->description,
				'unit_price' => $model->price,
				'login'  => $model->login
				);


				$result = $api->api_call_put($request, '', $array);

				if($result == 1){
					echo '<div class="alert alert-success">';
		      		echo '<p>Service edited!</p>';
					echo '</div>';
				}
				else {
					#echo $model->status;
					echo '<div class="alert alert-error">';
		      		echo '<p>Error! Massege: '. $api->response[0]['message'].'</p>';
					echo '</div>';
				}
		}
	}
?>

<?php echo Html::beginTag('div', ['class'=>'span3']); ?> 
	<?php echo "<h4> Edit ".$service." service (".$service_id.") for ID ".$id."</h4>"; ?>
	<?php $form = ActiveForm::begin()?>
	<?php 
		echo $form->field($model, 'status')->dropDownList([
			'active' => 'active',
			'disabled' => 'disabled',
			'hidden' =>'hidden',
			'pending' => 'pending'

			]); 
	?>
	<?php echo $form->field($model, 'description'); ?>
	<?php echo $form->field($model, 'price'); ?>
	<?php echo $form->field($model, 'login'); ?>
	<?php echo Html::submitButton('Save', ['class' => 'btn btn-success']);?>
	<?php $form = ActiveForm::end()?>
<?php echo Html::endTag('div');?>