<?php

	include './././scripts/autoresize.php';

	use yii\widgets\ActiveForm;
	use yii\helpers\Html;
	use kartik\dropdown\DropdownX;


	$request = 'admin/customers/customer/'.$id;

	$api->api_call_get($request);

	$profile = $api->response;

	if($_POST) {
		
		if($model->load(Yii::$app->request->post())){
			
			$array = array(
				'login' => $model->login,
				'password' => $model->password,
				'name' => $model->name,
				'status' => $model->status,
			);
			
			$result =  $api->api_call_put($request, '', $array);
			
			if($result == 1){
				echo '<div class="alert alert-success">';
	      		echo '<p>Changes successfully saved!</p>';
				echo '</div>';
			}
			else {
				print_r($api->response);
				echo '<div class="alert alert-error">';
	      		echo '<p>Error! Try again '. $api->response[0]['message'].'.</p>';
				echo '</div>';
			}
		}
	} 
?>

<?php echo Html::beginTag('div', ['class'=>'span5']); ?> 
	<?php echo "<h4> Profile ID ".$id."</h4>"; ?>
	<?php $form = ActiveForm::begin()?>
	<?php echo $form->field($model, 'login')->input('text', ['value' => $profile['login']]); ?>
	<?php echo $form->field($model, 'password')->input('password', ['value' => $profile['password']]); ?>
	<?php echo $form->field($model, 'name')->input('text', ['value' => $profile['name']]); ?>
	<?php echo $form->field($model, 'status')->dropDownList(['','new','active','disabled','blocked']); ?>
	<?php echo Html::submitButton('Save', ['class' => 'btn btn-success']);?>
	<?php $form = ActiveForm::end()?>

<?php echo Html::endTag('div');?>