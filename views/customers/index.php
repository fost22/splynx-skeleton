<div class="row">
		<div class="span9">
			<table class="table table-bordered">
    			<thead>
				    <tr>
				      <th>ID</th>
				      <th>Name</th>
				      <th>Status</th>
				      <th>Services</th>
				    </tr>
				  </thead>
				  <tbody>
				 	<?php 

						include './././scripts/autoresize.php';

						//Формуємо url запрос
						$request = "admin/customers/customer";

						//Викликаємо методом для запроса
						$result = $api->api_call_get($request);
						// Цикл для виведення всіх кастомерів в таблицю
						for($i = 0; $i < count($api->response); $i++){
							echo "<tr>";
							echo "<td>".$api->response[$i]['id']."</td>";
							echo "<td><a href='index.php?r=customers/profile&id=".$api->response[$i]['id']."'>".$api->response[$i]['name']."</a></td>";
							echo "<td>".$api->response[$i]['status']."</td>";
							echo "<td><a href='index.php?r=customers/services&id=".$api->response[$i]['id']."'>Edit/View</a></td>";
							echo "</tr>";
						}
					?>
				  </tbody>
    		</table>
		</div>
	</div>