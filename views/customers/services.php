<?php

	include './././scripts/autoresize.php';

	$ids = $id;

	$request_internet = 'admin/customers/customer/'.$id.'/internet-services'; // 
																			  //
	$api->api_call_get($request_internet);									  // Internet request
																			  //
	$internet = $api->response;												  //

	$request_voice = 'admin/customers/customer/'.$id.'/voice-services';		  //
																			  //
	$api->api_call_get($request_voice);										  // Voice request
																			  //
	$voice = $api->response;												  //

	$request_custom = 'admin/customers/customer/'.$id.'/custom-services';	  //
																			  //
	$api->api_call_get($request_custom);									  // Custom request
																			  //
	$custom = $api->response;												  //


	if($act == 'del'){

		if($service == 'internet'){
			$req = $request_internet.'--'.$service_id;
			$api->api_call_delete($req,$id=null);
			$id = $ids;
	
		}
		if($service == 'voice'){
			$req = $request_voice.'--'.$service_id;
			$api->api_call_delete($req, $id=null);
			$id = $ids;
		}
		if($service == 'custom'){
			$req = $request_custom.'--'.$service_id;
			$api->api_call_delete($req,$id=null);
			$id = $ids;
		}
		
	}


?>
<div class="row">
		<div class="span10">
			<h3>Internter services</h3>
			<div class="buttonH">	
				<?php 
					//Кнопка Add, для добавлення сервіса
					echo "<a href='index.php?r=customers/add-service&id=".$id."&service=internet' class='btn btn-primary'>Add</a>";
				?>
			</div>
			<form method="GET" action="services.php">
				<table class="table table-bordered">
	    			<thead>
					    <tr>
					      <th>ID</th>
					      <th>Status</th>
					      <th>Description</th>
					      <th>Price</th>
					      <th>Login</th>
					      <th>Action</th>
					    </tr>
					  </thead>
					  <tbody>
					 	<?php
							// Цикл для виведення всіх Internet сервісів кастомера в таблицю		
							for($i = 0; $i < count($internet); $i++){
								echo "<tr>";
								echo "<td>".$internet[$i]['id']."</td>";
								echo "<td>".$internet[$i]['status']."</td>";
								echo "<td>".$internet[$i]['description']."</td>";
								echo "<td>".$internet[$i]['unit_price']."</td>";
								echo "<td>".$internet[$i]['login']."</td>";
								echo "<td>
									<a href='index.php?r=customers/edit-service&id=".$id."&service=internet&service_id=".$internet[$i]['id']."' class='btn btn-mini'>Edit</a>
									<a href='index.php?r=customers/services&id=".$id."&act=del&service=internet&service_id=".$internet[$i]['id']."'class='btn btn-mini btn-danger'>Delete</a></td>";
								echo "</tr>";
							}
						?>
					  </tbody>
	    		</table>
    		<h3>Voice service</h3>
    		<div class="buttonH">
				<?php 
					//Кнопка Add, для добавлення сервіса
					echo "<a href='index.php?r=customers/add-service&id=".$id."&service=voice' class='btn btn-primary'>Add</a>";
				?>
			</div>
    		<table class="table table-bordered">
    			<thead>
				    <tr>
				      <th>ID</th>
				      <th>Status</th>
				      <th>Description</th>
				      <th>Price</th>
				      <th>Action</th>
				    </tr>
				  </thead>
				  <tbody>
				 	<?php 

						// Цикл для виведення всіх Voice сервісів кастомера в таблицю
						for($i = 0; $i < count($voice); $i++){
							echo "<tr>";
							echo "<td>".$voice[$i]['id']."</td>";
							echo "<td>".$voice[$i]['status']."</td>";
							echo "<td>".$voice[$i]['description']."</td>";
							echo "<td>".$voice[$i]['unit_price']."</td>";
							echo "<td>
									<a href='index.php?r=customers/edit-service&id=".$id."&service=voice&service_id=".$voice[$i]['id']."' class='btn btn-mini'>Edit</a>
									<a href='index.php?r=customers/services&id=".$id."&act=del&service=voice&service_id=".$voice[$i]['id']."' class='btn btn-mini btn-danger'>Delete</a></td>";
							echo "</tr>";
						}
					?>
				  </tbody>
    		</table>
    		<h3>Custom service</h3>
    		<div class="buttonH">
				<?php 
					//Кнопка Add, для добавлення сервіса
					echo "<a href='index.php?r=customers/add-service&id=".$id."&service=custom' class='btn btn-primary'>Add</a>";
				?>
			</div>
    		<table class="table table-bordered">
    			<thead>
				    <tr>
				      <th>ID</th>
				      <th>Status</th>
				      <th>Description</th>
				      <th>Price</th>
				      <th>Action</th>
				    </tr>
				  </thead>
				  <tbody>
				 	<?php 

						// Цикл для виведення всіх Custom сервісів кастомера в таблицю
						for($i = 0; $i < count($custom); $i++){
							echo "<tr>";
							echo "<td>".$custom[$i]['id']."</td>";
							echo "<td>".$custom[$i]['status']."</td>";
							echo "<td>".$custom[$i]['description']."</td>";
							echo "<td>".$custom[$i]['unit_price']."</td>";
							echo "<td>
									<a href='index.php?r=customers/edit-service&id=".$id."&service=custom&service_id=".$custom[$i]['id']."' class='btn btn-mini'>Edit</a>
									<a href='index.php?r=customers/services&id=".$id."&act=del&service=custom&service_id=".$custom[$i]['id']."' class='btn btn-mini btn-danger'>Delete</a></td>";
							echo "</tr>";
						}
					?>
				  </tbody>
    		</table>
    	</form>
		</div>
	</div>