<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\ProfileForm;
use app\models\AddServiceForm;
use app\models\EditServiceForm;

class CustomersController extends Controller
{


	public $layout = 'basic';
	
	public function actionIndex()
	{

		return $this->render('index');

	}

	public function actionProfile($id)
	{

		$model = new ProfileForm();

		return $this->render('profile', compact('id', 'model'));
	}

	public function actionServices($id, $act=null,$service=null,$service_id=null)
	{

		return $this->render('services', compact('id','act','service','service_id'));
	}

	public function actionAddService($id,$service=null)
	{
		$model = new AddServiceForm();

		return $this->render('addservice', compact('id','service', 'model'));
	}

	public function actionEditService($id,$service=null,$service_id=null)
	{
		$model = new EditServiceForm();

		return $this->render('editservice', compact('id','service','service_id', 'model'));
	}
}