<?php 

namespace app\models;

use yii\base\Model;


class ProfileForm extends Model
{
	
	public $login;
	public $password;
	public $name;
	public $status;

	public function rules()
	{

		return [
			[['login', 'name', 'password', 'status'], 'required'],
			['password','string','min' => 5]
		];
	}
}



