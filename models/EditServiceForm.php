<?php 

namespace app\models;

use yii\base\Model;


class EditServiceForm extends Model
{
	
	public $status;
	public $description;
	public $price;
	public $login;

	public function rules()
	{

		return [
			[['status', 'description', 'price', 'login'], 'required'],
			['description','string','min' => 4]
		];
	}
}