<?php 

namespace app\models;

use yii\base\Model;


class AddServiceForm extends Model
{
	public $tariff_id;
	public $tariff_login;
	public $status;
	public $description;
	public $price;
	public $phone;

	public function rules()
	{

		return [
			[['tariff_id','tariff_login', 'status', 'description', 'price', 'phone'], 'required']
		];
	}
}